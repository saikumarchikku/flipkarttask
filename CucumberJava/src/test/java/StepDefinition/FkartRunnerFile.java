package StepDefinition;

import java.util.Set;
import java.util.Iterator;



import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.*;

public class FkartRunnerFile {

	
	WebDriver driver=null;
	
	@Given("user Open the browser")
	public void user_open_the_browser() {
		System.setProperty("webdriver.chrome.driver", "D:\\java\\CucumberJava\\src\\test\\resources\\Drivers\\chromedriver.exe");
     	driver=new ChromeDriver();
     	driver.manage().window().maximize();
	}

	@And("user enters the Url and navigate to the home page")
	public void user_enters_the_url_and_navigate_to_the_home_page() {
		driver.get("https://www.flipkart.com/");
	    
	}

	@When("user enter the valid login credentials to login")
	public void user_enter_the_valid_login_credentials_to_login() {
		driver.findElement(By.xpath("//input[@class='_2IX_2- VJZDxU']")).sendKeys("saikumaar005@gmail.com");
		driver.findElement(By.xpath("//input[@class='_2IX_2- _3mctLh VJZDxU']")).sendKeys("Mama@005");
		driver.findElement(By.xpath("//button[@class='_2KpZ6l _2HKlqd _3AWRsL']")).click();
	    
	}

	@And("user search for require item and it must be navigated to searched result page")
	public void user_search_for_require_item_and_it_must_be_navigated_to_searched_result_page() throws InterruptedException {
	 driver.findElement(By.name("q")).sendKeys("samsung mobiles",Keys.ENTER);
	 Thread.sleep(2000);
	 
	 
	 
	}

	@Then("user select the item addd to cart")
	public void user_select_the_item_addd_to_cart() {
		driver.findElement(By.xpath("//div[text()='SAMSUNG Galaxy F41 (Fusion Blue, 128 GB)']")).click();
		Set<String> set=driver.getWindowHandles();
		Iterator<String> it=set.iterator();
		String MainWindow =it.next();
		String NewWindow =it.next();
		System.out.println("MainWindow ID=" +MainWindow);
		System.out.println("NewWindow Id ="+NewWindow);
		driver.switchTo().window(NewWindow);
		driver.findElement(By.xpath("//button[@class='_2KpZ6l _2U9uOA _3v1-ww']")).click();
		driver.close();
		driver.switchTo().window(MainWindow);
		
	    
	}
}
