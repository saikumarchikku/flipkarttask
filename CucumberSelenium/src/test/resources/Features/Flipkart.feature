Feature: Feature test for login into flipkart and add item into cart

  Scenario: 
    Given user entered into browser
    And user entered the url
    When user now in home page
    When user is click on the login button and user will login with valid credentials
    When user search for items in the search bar
    And page should be navigated into searched results
    And add the item to cart
    Then User is on MY Cart
