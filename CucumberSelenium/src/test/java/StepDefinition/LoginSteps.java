package StepDefinition;

import io.cucumber.java.en.*;

public class LoginSteps {
	@Given("user is on login page")
	public void user_is_on_login_page() {
		System.out.println("user is enterded into loginpage");
	}

	@When("user is entered valid login credentilas")
	public void user_is_entered_valid_login_credentilas() {
		System.out.println("user entered his valid credentials");
	}

	@And("click on ok button")
	public void click_on_ok_button() {
		System.out.println("user is clicked on OK button");
	}

	@Then("use must be navigated to home page")
	public void use_must_be_navigated_to_home_page() {
		System.out.println("user is navigated to homepage");
	}

}
