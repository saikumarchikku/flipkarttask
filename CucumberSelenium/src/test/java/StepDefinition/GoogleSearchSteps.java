package StepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;

public class GoogleSearchSteps {
	WebDriver driver=null;
	
	@Given("user is opened browser")
	public void user_is_opened_browser() {
		System.out.println("inside the step-browser is open");
		System.setProperty("webdriver.chrome.driver","D:/java/CucumberSelenium/src/test/resources/driver/chromedriver.exe");
		
		driver=new ChromeDriver();
		driver.manage().window().maximize();
	}

	@And("user is on google search page")
	public void user_is_on_google_search_page() {
		System.out.println("inside the step - user is on google search page");
		driver.navigate().to("https://www.google.com/");
	}

	@When("user enter the text in text box")
	public void user_enter_the_text_in_text_box() {
		System.out.println("inside the step- user enter the input text");
		driver.findElement(By.name("q")).sendKeys("testing");
	}
	@And("user hits enter")
	public void user_hits_enter() {
		System.out.println("inside the step- user hits the enter");
		driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
	}

	@Then("user is navigated to search results")
	public void user_is_navigated_to_search_results() {
		System.out.println("inside the step- user navigated into serach results");
		driver.getPageSource().contains("covid-19");
		
	}

}
