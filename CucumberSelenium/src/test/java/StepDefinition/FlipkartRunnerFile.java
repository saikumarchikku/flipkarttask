package StepDefinition;

import org.openqa.selenium.By;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.cucumber.java.en.*;

public class FlipkartRunnerFile {
	   WebDriver driver=null;
	   
	
	@Given("user entered into browser")
	public void user_entered_into_browser() {
		System.out.println("inside the step-browser is open");
		System.setProperty("webdriver.chrome.driver","D:/java/CucumberSelenium/src/test/resources/driver/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
	}

	@And("user entered the url")
	public void user_entered_the_url() {
		driver.get("https://www.flipkart.com/");
	}

	@When("user now in home page")
	public void user_now_in_home_page() {
		System.out.println("USER IS ON HOME PAGE");	  
	    
	}

	@When("user is click on the login button and user will login with valid credentials")
	public void user_is_click_on_the_login_button_and_user_will_login_with_valid_credentials() {
	   driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[1]/input")).sendKeys("saikumaar005@gmail.com");
	   driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[2]/input")).sendKeys("Mama@005");
	   driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[4]/button")).click();
	}

	@When("user search for items in the search bar")
	public void user_search_for_items_in_the_search_bar() {
		driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[2]/form/div/div/input")).sendKeys("iphone 11");
		
	   
	}

	@And("page should be navigated into searched results")
	public void page_should_be_navigated_into_searched_results() {
		driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[2]/form/div/div/input")).sendKeys(Keys.ENTER);
	}

	@When("add the item to cart")
	public void add_the_item_to_cart() {
	    driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[2]/div[2]/div/div/div/a/div[2]/div[1]/div[1]"));
	    driver.findElement(By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[1]/div[2]/div/ul/li[1]/button")).click();
	   
	}
	@Then("User is on MY Cart")
	public void user_is_on_my_cart() {
	    
	}
}
